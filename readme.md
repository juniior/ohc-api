<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## comandos
- composer install
- php artisan key:generate
- php artisan migrate --force
- php artisan db:seed --force
- php artisan passport:install --force
- php artisan serve
- php artisan storage:link
- php artisan migrate --force && php artisan db:seed --force && php artisan passport:install --force

## Permissões

- chown -R www-data.www-data /var/www/ohc-api/storage
- chown -R www-data.www-data /var/www/ohc-api/bootstrap/cache
- chown -R www-data.www-data /var/www/ohc-api/vendor/mpdf/mpdf

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
