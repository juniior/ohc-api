<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Membro extends Model
{
    protected $table = 'membros';

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['user_id', 'orquestra_id', 'role_id', 'instrumento_id', 'is_default'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orquestra()
    {
        return $this->belongsTo(Orquestra::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function instrumento()
    {
        return $this->belongsTo(Instrumento::class);
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i');
    }

}
