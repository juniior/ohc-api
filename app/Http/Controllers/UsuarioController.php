<?php

namespace App\Http\Controllers;

use App\Membro;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\User;
use Auth, Validator, Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class UsuarioController extends Controller
{
    protected $usuario;

    public function __construct(User $usuario)
    {
        $this->usuario = $usuario;
    }

    public function login(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'email' => 'required|string|email|max:200',
            'password' => 'required|string'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors(), Response::HTTP_OK);
        }
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $user = auth()->user();
            $user->token = $user->createToken($user->email)->accessToken;
            $perfis = Membro::whereUserId($user->id)->with(['orquestra', 'role', 'instrumento'])->orderBy('orquestra_id')->get();

            if($user->photo)
                $user->photo  = asset('storage/' . $user->photo);

            return $this->sendResponse(compact('user', 'perfis'), 'Bem vindo!');
        } else {
            return $this->sendError('Credenciais inválidas', $validator->errors(), Response::HTTP_OK);
        }
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();

        return $this->sendResponse('OK', Response::HTTP_OK);
    }

    public function index()
    {
        return $this->sendResponse(User::orderBy('name')->paginate(25)->toArray(), 'Retrieved successfully.');
    }

    public function trashed()
    {
        return $this->sendResponse(User::onlyTrashed()->orderBy('name')->paginate(25)->toArray(), 'Usuarios retrieved successfully.');
    }

    public function destroy($id)
    {
        User::destroy($id);
        return $this->sendResponse(User::orderBy('name')->paginate(5)->toArray(), Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
//            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors(), Response::HTTP_OK);
        }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'dt_nasc' => $data['dt_nasc'],
            'sexo' => $data['sexo'],
            'phone' => $data['phone'],
            'password' => Hash::make('123123'),
        ]);

        $user->token = $user->createToken($user->email)->accessToken;

        return $this->sendResponse($user->toArray(), 'Registrado com sucesso!', Response::HTTP_OK);
    }

    public function cadastro(Request $request)
    {
        $data = $request->all();


        $validator = Validator::make($data, [
            'name' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors(), Response::HTTP_OK);
        }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'dt_nasc' => $data['dt_nasc'],
            'sexo' => $data['sexo'],
            'phone' => $data['phone'],
            'password' => Hash::make($data['password']),
        ]);

        Membro::create([
            'user_id' => $user->id,
            'orquestra_id' => null,
            'role_id' => 4,
            'instrumento_id' => null,
            'is_default' => true,
        ]);

        $user->token = $user->createToken($user->email)->accessToken;

        return $this->sendResponse(compact('user'), 'Usuario created successfully.');
    }

    public function update(Request $request, User $usuario)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|string',
        ]);

        if ($validator->fails())
        return $this->sendError('Validation Error.', $validator->errors(), Response::HTTP_OK);


        $usuario->name = $request->name;
        $usuario->dt_nasc = $request->dt_nasc;
        $usuario->sexo = $request->sexo;
        $usuario->phone = $request->phone;
        $usuario->save();

        $perfis = Membro::whereUserId($usuario->id)->with(['orquestra', 'role', 'instrumento'])->orderBy('orquestra_id')->get();

        if($usuario->photo)
            $usuario->photo  = asset('storage/' . $usuario->photo);

        return $this->sendResponse(compact('usuario', 'perfis'), 'Perfil alterado com sucesso');
    }

    public function show(User $usuario)
    {

        if (is_null($usuario))
            return $this->sendError('Usuário not found.', null, Response::HTTP_OK);

        $usuario->photo  = asset('storage/' . $usuario->photo);

        return $this->sendResponse($usuario->toArray(), 'Usuário retrieved successfully.');
    }

    public function restore($id)
    {
        $user = User::withTrashed()
            ->where('id', $id)
            ->restore();

        return $this->sendResponse($user, 'Usuario restore successfully.');
    }

    public function upload(Request $request, User $usuario)
    {

        $file = $request->file('imgUsuario');

        $pasta_imagens = 'usuario/'. $usuario->id . '/imagens/';

        $filename = $pasta_imagens . time() . '.' . $file->getClientOriginalExtension();
        if (Storage::disk('public')->put($filename, File::get($file))) {
            $usuario->photo = $filename;
            $usuario->save();
        }

        $usuario->photo  = asset('storage/' . $usuario->photo);

        return $this->sendResponse($usuario, Response::HTTP_OK);


    }

}
