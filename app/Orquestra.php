<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Orquestra extends Model
{
    protected $table = 'orquestras';

    use SoftDeletes;
    protected $dates = ['deleted_at', 'dt_end'];

    protected $fillable = [
        'name', 'acronyms', 'address', 'email', 'phone', 'photo', 'codigo'
    ];

    public function getDtEndAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function setDtEndAttribute($value)
    {
        $this->attributes['dt_end'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }

    public function membros() {
        return $this->hasMany(Membro::class);
    }

    public function musicas() {
        return $this->hasMany(Musica::class);
    }

    public function eventos() {
        return $this->hasMany(Evento::class);
    }
}
