<?php

use Illuminate\Database\Seeder;

class MusicaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('musicas')->delete();

        $faker = Faker\Factory::create();

        for($i = 0; $i < 20; $i++) {
            App\Musica::create([
                'tonality' => $faker->randomElement($array = array ('C','Cm','E','F','A','G','B','D')),
                'name' => $faker->name,
                'orquestra_id' => $faker->numberBetween($min = 1, $max = 2),
                'composer' => $faker->name,
                'letter' => $faker->text,
            ]);
        }

    }
}
