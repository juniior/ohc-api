<?php

namespace App\Http\Controllers;

use App\Role;
use Auth, Validator, Hash;

class RoleController extends Controller
{
    public function index()
    {
        return $this->sendResponse(Role::orderBy('name')->select('id', 'name')->get(), '');
    }

    public function list() {
        return $this->sendResponse(Role::orderBy('name')->select('id', 'name')->get(), '');
    }
}
