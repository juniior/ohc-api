<?php

namespace App\Http\Controllers;

use App\Evento;
use App\Membro;
use App\Orquestra;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth, Validator, Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class OrquestraController extends Controller
{
    protected $orquestra;

    public function __construct(Orquestra $orquestra)
    {
        $this->orquestra = $orquestra;
    }

    public function index()
    {
        return $this->sendResponse(Orquestra::orderBy('name')->paginate(25)->toArray(), 'Orquestras retrieved successfully.');
    }

    public function show(Orquestra $orquestra)
    {

        if (is_null($orquestra)) {
            return $this->sendError('Orquestra not found.');
        }
        if ($orquestra->photo)
            $orquestra->photo = asset('storage/' . $orquestra->photo);

        $orquestra->membros = DB::table('users')
            ->join('membros', 'users.id', '=', 'membros.user_id')
            ->whereOrquestraId($orquestra->id)
            ->select('users.id as user_id', 'users.name')
            ->distinct()
            ->get();

        return $this->sendResponse($orquestra, 'Orquestra retrieved successfully.');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'email' => ['required', 'string', 'email', 'unique:orquestras', 'max:200'],
            'name' => 'required|string',
            'acronyms' => 'required|string|max:25'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $product = Orquestra::create([
            'acronyms' => $request->acronyms,
            'address' => $request->address,
            'email' => $request->email,
            'name' => $request->name,
            'phone' => $request->phone,
            'codigo' => md5(time()),
        ]);

        return $this->sendResponse($product->toArray(), 'Orquestra created successfully.');
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $orquestra = $this->orquestra->findOrFail($id);

        $validator = Validator::make($input, [
            'email' => ['required', 'string', 'email', 'max:200', Rule::unique('orquestras')->ignore($orquestra->id)],
            'name' => 'required|string',
            'acronyms' => 'required|string|max:25'
        ]);

        if ($validator->fails())
            return $this->sendError('Validation Error.', $validator->errors());

        $orquestra->acronyms = $request->acronyms;
        $orquestra->address = $request->address;
        $orquestra->email = $request->email;
        $orquestra->name = $request->name;
        $orquestra->phone = $request->phone;
        $orquestra->save();

        return $this->sendResponse(compact('orquestra'), 'Orquestra update successfully.');
    }

    public function destroy($id)
    {
        Orquestra::destroy($id);
        return $this->sendResponse(Orquestra::orderBy('name')->paginate(25)->toArray(), Response::HTTP_OK);
    }

    public function upload(Request $request, Orquestra $orquestra)
    {
        $file = $request->file('imgOrquestra');

        $pasta_imagens = 'orquestra/' . $orquestra->id . '/imagens/';

        $filename = $pasta_imagens . time() . '.' . $file->getClientOriginalExtension();
        if (Storage::disk('public')->put($filename, File::get($file))) {
            $orquestra->photo = $filename;
            $orquestra->save();
        }

        $orquestra->photo = asset('storage/' . $orquestra->photo);

        return $this->sendResponse($orquestra, Response::HTTP_OK);
    }

    public function allEventos(Orquestra $orquestra)
    {
        $eventos = Evento::whereOrquestraId($orquestra->id)->orderBy('dt_evento', 'desc')->select('id', 'name', 'maestro', 'local', 'dt_evento', 'horario')->get();
        return $this->sendResponse($eventos, Response::HTTP_OK);
    }

}
