<?php

namespace App\Http\Controllers;

use App\Instrumento;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class InstrumentoController extends Controller
{
    protected $instrumento;

    public function __construct(Instrumento $instrumento)
    {
        $this->instrumento = $instrumento;
    }

    public function index()
    {
        return $this->sendResponse(Instrumento::orderBy('name')->paginate(25)->toArray(), 'Instrumentos retrieved successfully.');
    }


    public function trashed()
    {
        return $this->sendResponse(Instrumento::onlyTrashed()->orderBy('name')->paginate(25)->toArray(), 'Instrumentos retrieved successfully.');
    }

    public function show($id)
    {
        $instrumento = Instrumento::find($id);

        if (is_null($instrumento)) {
            return $this->sendError('Instrumento não encontrado. instrumento.show');
        }

        return $this->sendResponse($instrumento->toArray(), 'Instrumento retrieved successfully.');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|string',
            'tonality' => 'required|string|max:25'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $product = Instrumento::create([
            'name' => $request->name,
            'description' => $request->description,
            'tonality' => $request->tonality
        ]);

        return $this->sendResponse(Instrumento::orderBy('name')->select('id', 'name', 'tonality')->get(), 'Instrumento created successfully.');
    }

    public function update(Request $request, Instrumento $instrumento)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|string',
            'tonality' => 'required|string|max:25'
        ]);

        if ($validator->fails())
            return $this->sendError('Validation Error.', $validator->errors());

        $instrumento->tonality = $request->tonality;
        $instrumento->description = $request->description;
        $instrumento->name = $request->name;
        $instrumento->save();

        return $this->sendResponse(Instrumento::orderBy('name')->select('id', 'name', 'tonality')->get(), 'Instrumento update successfully.');
    }

    public function destroy($id)
    {
        Instrumento::destroy($id);
        return $this->sendResponse(Instrumento::orderBy('name')->select('id', 'name', 'tonality')->get(), Response::HTTP_OK);
    }

    public function restore($id)
    {
        $instrumento = Instrumento::withTrashed()
            ->where('id', $id)
            ->restore();

        return $this->sendResponse(Instrumento::orderBy('name')->select('id', 'name', 'tonality')->get(), 'Instrumento restore successfully.');
    }

    public function list() {
        return $this->sendResponse(Instrumento::all()->toArray(), '');
    }
}
