<?php

use Illuminate\Database\Seeder;

class EventoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('eventos')->delete();

        DB::table('eventos')->insert([
            'user_id' => 1,
            'orquestra_id' => 1,
            'name' => 'Ensaio - Recital',
            'maestro' => 'Mario Sidney',
            'local' => 'IEAD - Central',
            'dt_evento' => '2019-12-25',
            'horario' => '17:00:00',
            'observacao' => 'Hinos da H.C.: 20; 292; 18.',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('eventos')->insert([
            'user_id' => 1,
            'orquestra_id' => 1,
            'name' => 'Culto - EBD',
            'maestro' => 'Mario Sidney',
            'local' => 'IEAD - Central',
            'dt_evento' => '2019-12-25',
            'horario' => '20:00:00',
            'observacao' => 'Hinos da H.C.: 20; 292; 18.',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('eventos')->insert([
            'user_id' => 1,
            'orquestra_id' => 1,
            'name' => 'Ensaio Cantata',
            'maestro' => 'Rafael Leite',
            'local' => 'IEAD - Central',
            'dt_evento' => '2019-12-25',
            'horario' => '22:00:00',
            'observacao' => 'Hinos da H.C.: 20; 292; 18.',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('eventos')->insert([
            'user_id' => 1,
            'orquestra_id' => 1,
            'name' => 'AUDIÇÃO OHC 2019 / NAIPE CORDAS',
            'maestro' => 'Uílias Rego Rocha',
            'local' => 'Sala do Departamento Musical - IEAD PVH',
            'dt_evento' => '2019-12-26',
            'horario' => '17:00:00',
            'observacao' => 'Hinos da H.C.: 20; 292; 18.',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('eventos')->insert([
            'user_id' => 1,
            'orquestra_id' => 1,
            'name' => 'Culto -  Santa Ceia',
            'maestro' => 'Rafael Leite',
            'local' => 'IEAD - Central',
            'dt_evento' => '2019-12-27',
            'horario' => '17:00:00',
            'observacao' => 'Hinos da H.C.: 20; 292; 18.',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('eventos')->insert([
            'user_id' => 1,
            'orquestra_id' => 1,
            'name' => 'Culto - EBD',
            'maestro' => 'Héber Tarses',
            'local' => 'IEAD - Central',
            'dt_evento' => '2019-12-28',
            'horario' => '17:00:00',
            'observacao' => 'Hinos da H.C.: 20; 292; 18.',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);



        DB::table('eventos')->insert([
            'user_id' => 1,
            'orquestra_id' => 1,
            'name' => 'Culto -  Missões',
            'maestro' => 'Rafael Leite',
            'local' => 'IEAD - Central',
            'dt_evento' => '2019-12-05',
            'horario' => '17:00:00',
            'observacao' => 'Hinos da H.C.: 20; 292; 18.',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('eventos')->insert([
            'user_id' => 1,
            'orquestra_id' => 1,
            'name' => 'Ensaio - EBD',
            'maestro' => 'Héber Tarses',
            'local' => 'IEAD - Central',
            'dt_evento' => '2019-12-17',
            'horario' => '16:50:00',
            'observacao' => 'Hinos da H.C.: 20; 292; 18.',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
