<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(OrquestraTableSeeder::class);
        $this->call(InstrumentoTableSeeder::class);
        $this->call(UsuarioTableSeeder::class);
        $this->call(MembroTableSeeder::class);
        $this->call(MusicaTableSeeder::class);
        $this->call(EventoTableSeeder::class);

    }
}
