<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartituraInstrumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partitura_instrumentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('instrumento_id')->unsigned();
            $table->integer('partitura_id')->unsigned();

            $table->foreign('instrumento_id')->references('id')->on('instrumentos');
            $table->foreign('partitura_id')->references('id')->on('partituras');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partitura_instrumentos');
    }
}
