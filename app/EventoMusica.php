<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventoMusica extends Model
{
    protected $table = 'evento_musicas';
    protected $fillable = ['ordem', 'evento_id', 'musica_id'];

    public function musica() {
		return $this->belongsTo(Musica::class);
	}
	public function evento() {
		return $this->belongsTo(Evento::class);
	}
}
