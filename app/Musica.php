<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;


class Musica extends Model
{
    protected $table = 'musicas';

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'tonality', 'composer', 'letter', 'audio', 'orquestra_id'];

    public function orquestra() {
        return $this->belongsTo(Orquestra::class);
    }

//    public function partituras() {
//
//        $partituras = DB::table('musicas')
//            ->join('partituras', 'partituras.musica_id', '=', 'musicas.id')
//            ->join('partitura_instrumentos', 'partitura_instrumentos.partitura_id', '=', 'partituras.id')
//            ->join('instrumentos', 'instrumentos.id', '=', 'partitura_instrumentos.instrumento_id')
//            ->where('musicas.id', '=', $this->id)
//            ->whereNull('partituras.deleted_at')
//            ->select('partitura_instrumentos.id', 'partituras.file','partituras.id AS partitura_id', 'instrumentos.id AS instrumento_id', 'instrumentos.name', 'partituras.binario')
//            ->orderBy('instrumentos.name')
//            ->get();
//
//        return $partituras;
//    }

    public function partituras()
    {
        return $this->hasMany(Partitura::class)->with(['instrumentos']);

    }



}
