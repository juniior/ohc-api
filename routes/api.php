<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Support\Facades\Storage;

Route::post('/login', 'UsuarioController@login')->name('usuario.login');
Route::post('/cadastro', 'UsuarioController@cadastro')->name('usuario.cadastro');

Route::middleware('auth:api')->get('/usuario/trashed', 'UsuarioController@trashed')->name('usuario.trashed');
Route::middleware('auth:api')->put('/usuario/{id}/restore', 'UsuarioController@restore')->name('usuario.restore');
Route::middleware('auth:api')->resource('usuario', 'UsuarioController')->except([]);
Route::middleware('auth:api')->post('/usuario/{usuario}/upload', 'UsuarioController@upload')->name('usuario.upload');

Route::middleware('auth:api')->post('/usuario/logout', 'UsuarioController@logout')->name('usuario.logout');


Route::middleware('auth:api')->resource('orquestra', 'OrquestraController');
Route::middleware('auth:api')->post('/orquestra/{orquestra}/upload', 'OrquestraController@upload')->name('orquestra.upload');
Route::middleware('auth:api')->get('/orquestra/{orquestra}/eventos', 'OrquestraController@allEventos')->name('orquestra.eventos');

Route::middleware('auth:api')->get('/instrumento/trashed', 'InstrumentoController@trashed')->name('instrumento.trashed');
Route::middleware('auth:api')->put('/instrumento/{id}/restore', 'InstrumentoController@restore')->name('instrumento.restore');
Route::middleware('auth:api')->resource('instrumento', 'InstrumentoController');
Route::middleware('auth:api')->get('/instrumento/list', 'InstrumentoController@list')->name('instrumento.list');

Route::middleware('auth:api')->resource('role', 'RoleController')->except([
    'create', 'store', 'update', 'destroy', 'show', 'edit'
]);

Route::middleware('auth:api')->get('/role/list', 'RoleController@list')->name('role.list');

Route::middleware('auth:api')->resource('membro', 'MembroController');
Route::middleware('auth:api')->get('/membro/{id}/orquestra', 'MembroController@membrosOrquestra')->name('membro.musicos');
Route::middleware('auth:api')->post('/membro/perfis', 'MembroController@perfis')->name('membro.perfis');
Route::middleware('auth:api')->post('/musicos', 'MembroController@musicosOrquestra')->name('membro.musicos');

Route::middleware('auth:api')->resource('solicitacao', 'SolicitacaoController');
Route::middleware('auth:api')->get('/solicitacao/listaPorUsuario/{user}', 'SolicitacaoController@listaPorUsuario')->name('solicitacao.listaPorUsuario');
Route::middleware('auth:api')->get('/solicitacao/listaPorOrquestra/{orquestra}', 'SolicitacaoController@listaPorOrquestra')->name('solicitacao.listaPorOrquestra');

Route::middleware('auth:api')->post('/solicitacao/aceitar', 'SolicitacaoController@aceitar')->name('solicitacao.aceitar');
Route::middleware('auth:api')->post('/solicitacao/recusar', 'SolicitacaoController@recusar')->name('solicitacao.recusar');

Route::middleware('auth:api')->resource('musica', 'MusicaController');
Route::middleware('auth:api')->get('/musica/listaPorOrquestra/{orquestra}', 'MusicaController@listaPorOrquestra')->name('musica.listaPorOrquestra');
Route::middleware('auth:api')->get('/musica/{musica}/partituras', 'MusicaController@allPartituras')->name('musica.partituras');

Route::middleware('auth:api')->resource('partitura', 'PartituraController');

Route::middleware('auth:api')->resource('evento', 'EventoController');
Route::middleware('auth:api')->post('/evento/lista', 'EventoController@lista')->name('evento.lista');
Route::middleware('auth:api')->get('/evento/{evento}/musicas', 'EventoController@allMusicas')->name('evento.musicas');
Route::middleware('auth:api')->post('/evento/musica', 'EventoController@addMusica')->name('evento.musica');
Route::middleware('auth:api')->get('/evento/{EventoMusica}/remove', 'EventoController@removeMusica')->name('evento.removeMusica');

Route::middleware('auth:api')->get('/evento/{evento}/arranjo', 'EventoController@geraPdf')->name('evento.arranjo');
Route::middleware('auth:api')->get('/evento/{evento}/arranjo/{instrumento}', 'EventoController@geraPdf')->name('evento.arranjo.instrumento');
