<?php

use Illuminate\Database\Seeder;

class MembroTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('membros')->delete();

        // Administrador master
        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => null,
            'role_id' => 1,
            'instrumento_id' => null,
            'is_default' => true
        ]);

//        \App\Membro::create([
//            'user_id' => 1,
//            'orquestra_id' => null,
//            'role_id' => 4,
//            'instrumento_id' => null,
//            'is_default' => false
//        ]);

        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => 1,
            'role_id' => 1,
            'instrumento_id' => null,
            'is_default' => false
        ]);

        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => 2,
            'role_id' => 1,
            'instrumento_id' => null,
            'is_default' => false
        ]);

        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => 1,
            'role_id' => 4,
            'instrumento_id' => 2,
            'is_default' => false
        ]);

        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => 2,
            'role_id' => 4,
            'instrumento_id' => 1,
            'is_default' => false
        ]);


        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => 1,
            'role_id' => 4,
            'instrumento_id' => 1,
            'is_default' => false
        ]);

        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => 1,
            'role_id' => 3,
            'instrumento_id' => 1,
            'is_default' => false
        ]);

        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => 1,
            'role_id' => 3,
            'instrumento_id' => 2,
            'is_default' => false
        ]);

        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => 1,
            'role_id' => 3,
            'instrumento_id' => 3,
            'is_default' => false
        ]);

        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => 1,
            'role_id' => 3,
            'instrumento_id' => 4,
            'is_default' => false
        ]);

        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => 2,
            'role_id' => 3,
            'instrumento_id' => 3,
            'is_default' => false
        ]);

        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => 2,
            'role_id' => 3,
            'instrumento_id' => 4,
            'is_default' => false
        ]);

        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => 2,
            'role_id' => 3,
            'instrumento_id' => 5,
            'is_default' => false
        ]);

        \App\Membro::create([
            'user_id' => 1,
            'orquestra_id' => 2,
            'role_id' => 3,
            'instrumento_id' => 6,
            'is_default' => false
        ]);




        \App\Membro::create([
            'user_id' => 2,
            'orquestra_id' => 1,
            'role_id' => 1,
            'instrumento_id' => null,
            'is_default' => true
        ]);

        \App\Membro::create([
            'user_id' => 2,
            'orquestra_id' => 1,
            'role_id' => 4,
            'instrumento_id' => 3,
            'is_default' => false
        ]);

        \App\Membro::create([
            'user_id' => 2,
            'orquestra_id' => 1,
            'role_id' => 4,
            'instrumento_id' => null,
            'is_default' => false
        ]);

    }
}
