<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartituraInstrumento extends Model
{
    protected $table = 'partitura_instrumentos';

    protected $fillable = array('instrumento_id', 'partitura_id');

    public function instrumento() {
        return $this->belongsTo(Instrumento::class);
    }

    public function partitura() {
        return $this->belongsTo(Partitura::class);
    }

}
