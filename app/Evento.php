<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use DB;

class Evento extends Model
{
    protected $table = 'eventos';

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['user_id', 'name', 'maestro', 'local', 'dt_evento', 'horario', 'observacao', 'orquestra_id'];

    public function getDtEventoAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function setDtEventoAttribute($value)
    {
        $this->attributes['dt_evento'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }

    public function musicas() {
        return $this->belongsToMany(Musica::class, 'evento_musicas', 'evento_id', 'musica_id')->withPivot('id','ordem')->orderBy('ordem');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orquestra() {
        return $this->belongsTo(Orquestra::class);
    }

//    public function musicasTeste() {
//
//         $musicas = DB::table('evento_musicas')
//            ->join('eventos', 'eventos.id', '=', 'evento_musicas.evento_id')
//            ->join('musicas', 'musicas.id', '=', 'evento_musicas.musica_id')
//            ->where('eventos.id', '=', $this->id)
//            ->whereNull('eventos.deleted_at')
//            ->whereNull('musicas.deleted_at')
//            ->select('evento_musicas.id', 'evento_musicas.ordem','evento_musicas.musica_id', 'musicas.name', 'musicas.tonality', 'musicas.composer')
//            ->orderBy('evento_musicas.ordem')
//            ->get();
//
//        return $musicas;
//
//    }
}
