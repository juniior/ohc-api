<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartiturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partituras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('file')->nullable();
            $table->text('mime')->nullable();
            $table->integer('musica_id')->index()->unsigned();

            $table->softDeletes();
            $table->foreign('musica_id')->references('id')->on('musicas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partituras');
    }
}
