<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->integer('orquestra_id')->unsigned();
            $table->string('name');
            $table->string('maestro')->nullable();
            $table->string('local');
            $table->date('dt_evento');
            $table->time('horario');
            $table->longtext('observacao')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('orquestra_id')->references('id')->on('orquestras');

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
