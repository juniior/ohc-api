<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventoMusicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evento_musicas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ordem');
            $table->integer('evento_id')->unsigned();
            $table->integer('musica_id')->unsigned();

            $table->foreign('evento_id')->references('id')->on('eventos');
            $table->foreign('musica_id')->references('id')->on('musicas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evento_musicas');
    }
}
