<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partitura extends Model
{
    protected $table = 'partituras';

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['file', 'mime', 'musica_id', 'binario'];

    public function instrumentos() {
        return $this->belongsToMany(Instrumento::class, 'partitura_instrumentos', 'partitura_id', 'instrumento_id')->orderBy('instrumentos.name');
    }

    public function musica()
    {
        return $this->belongsTo(Musica::class, 'musica_id');
    }

}
