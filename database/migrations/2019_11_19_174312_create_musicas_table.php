<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMusicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('musicas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('tonality')->nullable();
            $table->string('composer')->nullable();
            $table->string('audio')->nullable();
            $table->longtext('letter')->nullable();
            $table->integer('orquestra_id')->nullable()->index()->unsigned();

            $table->foreign('orquestra_id')->references('id')->on('orquestras');

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('musicas');
    }
}
