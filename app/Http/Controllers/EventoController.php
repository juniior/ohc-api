<?php

namespace App\Http\Controllers;

use App\Evento;
use App\EventoMusica;
use App\Instrumento;
use App\Musica;
use App\Orquestra;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Validator, PDFMerger;
use PDF;

class EventoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lista()
    {
        return $this->sendResponse(Evento::all(), 'Retrieved successfully.');
    }

//    public function listaPorOrquestra(Orquestra $orquestra)
//    {
//        return $this->sendResponse($orquestra->eventos->toArray(), 'Retrieved successfully.');
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->evento, [
            'name' => 'required|string|max:200',
            'local' => 'required|string|max:200',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $evento = Evento::create([
            'name' => $request->evento['name'],
            'maestro' => $request->evento['maestro'],
            'local' => $request->evento['local'],
            'dt_evento' => $request->evento['dt_evento'],
            'horario' => $request->evento['horario'],
            'observacao' => $request->evento['observacao'],
            'orquestra_id' => $request->orquestra,
            'user_id' => 1,
        ]);

        return $this->sendResponse($evento->toArray(), Response::HTTP_NO_CONTENT);
    }

    public function update(Request $request, Evento $evento)
    {
        $evento->name = $request->name;
        $evento->maestro = $request->maestro;
        $evento->local = $request->local;
        $evento->dt_evento = $request->dt_evento;
        $evento->horario = $request->horario;
        $evento->observacao = $request->observacao;

        $evento->save();

        $eventos = Evento::whereOrquestraId($evento->orquestra_id)->orderBy('dt_evento', 'desc')->select('id', 'name', 'maestro', 'local', 'dt_evento', 'horario')->get();

        return $this->sendResponse(compact('evento', 'eventos'), Response::HTTP_NO_CONTENT);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Evento $evento
     * @return \Illuminate\Http\Response
     */
    public function show(Evento $evento)
    {
        $musicas = $evento->musicas->toArray();
        return $this->sendResponse(compact('evento', 'musicas'), Response::HTTP_NO_CONTENT);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Evento $evento
     * @return \Illuminate\Http\Response
     */
    public function edit(Evento $evento)
    {
        return $this->sendResponse($evento->toArray(), Response::HTTP_NO_CONTENT);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Evento $evento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Evento $evento)
    {
        $evento->delete();
        return $this->sendResponse(null, Response::HTTP_NO_CONTENT);
    }

    public function allMusicas(Evento $evento)
    {
        $musicas = $evento->musicas->toArray();
        return $this->sendResponse(compact('musicas'), Response::HTTP_NO_CONTENT);
    }

    public function addMusica(Request $request)
    {
        $em = EventoMusica::whereMusicaId($request->musica)->whereEventoId($request->evento)->get();
        if (count($em) == 0) {

            if ($request->ordem == null)
                $i = (EventoMusica::whereEventoId($request->evento)->count() + 1);
            else
                $i = $request->ordem;

            $em = EventoMusica::create([
                'ordem' => $i,
                'evento_id' => $request->evento,
                'musica_id' => $request->musica,
            ]);
        }

        $musicas = Evento::find($request->evento)->musicas->toArray();
        return $this->sendResponse(compact('em', 'musicas'), Response::HTTP_NO_CONTENT);
    }

    public function removeMusica($id)
    {
        $eventoMusica = EventoMusica::find($id);
        $eventoMusica->delete();
        $musicas = Evento::find($eventoMusica->evento_id)->musicas->toArray();
        return $this->sendResponse(compact('musicas'), Response::HTTP_NO_CONTENT);
    }

    public function geraPdf(Evento $evento, Instrumento $instrumento = null)
    {
        $pasta_capas = storage_path('app') . '/public/orquestra/' . $evento->orquestra_id . '/capas/';
        $pasta_eventos = storage_path('app') . '/public/orquestra/' . $evento->orquestra_id . '/eventos/';

        $arquivo = [];
        foreach ($evento->musicas as $musica) {
            foreach ($musica->partituras as $partitura) {
                if ($instrumento != null) {
                    foreach ($partitura->instrumentos as $inst) {
                        if ($inst->id == $instrumento->id)
                            $arquivo = Arr::add($arquivo, $partitura->id, $partitura->file);
                    }
                } else {
                    $arquivo = Arr::add($arquivo, $partitura->id, $partitura->file);
                }
            }
        }
        Storage::disk('public')->makeDirectory('orquestra/' . $evento->orquestra_id . '/eventos');
        $pdf = new PDFMerger();

        $this->geraCapa($evento, $pasta_capas, $instrumento);

        $pdf->addPDF($pasta_capas . 'evento-' . $evento->id . '.pdf', 'all');

        foreach ($arquivo as $item) {
            $pdf->addPDF(storage_path('app') . '/public/' . $item, 'all');
        }

        $binaryContent = $pdf->merge('string', "mergedpdf.pdf");

        $pdf->merge('file', $pasta_eventos . 'evento-' . $evento->id . '.pdf');

        return $this->sendResponse(base64_encode($binaryContent), Response::HTTP_NO_CONTENT);
    }

    private function geraCapa(Evento $evento, $pasta_capas, Instrumento $instrumento = null)
    {
        $data = [
            'evento' => $evento,
            'instrumento' => $instrumento,
            'foto' => asset('public/' . $evento->orquestra->photo)
        ];

        Storage::disk('public')->makeDirectory('orquestra/' . $evento->orquestra_id . '/capas');

        $pdf = PDF::loadView('pdf.capa', $data, [], [
            'title' => 'Another Title',
            'margin_top' => 80
        ]);

        return $pdf->save($pasta_capas . 'evento-' . $evento->id . '.pdf');
    }

}
