<?php

namespace App\Http\Controllers;

use App\Membro;
use App\Orquestra;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth, Validator, Hash;
use Illuminate\Support\Facades\DB;

class MembroController extends Controller
{
    protected $membro;

    public function __construct(Membro $membro)
    {
        $this->membro = $membro;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->sendResponse(Membro::orderBy('orquestra_id')->paginate(25)->toArray(), 'Retrieved successfully.');
    }

    public function membrosOrquestra($id)
    {
        $membros = DB::table('users')
            ->join('membros', 'users.id','=','membros.user_id')
            ->whereOrquestraId($id)
            ->select('users.*')
            ->distinct()
            ->get();

        return $this->sendResponse($membros->toArray(), 'Retrieved successfully.');
    }

    public function musicosOrquestra(Request $request)
    {
        $data =  $request->all();

        $membros = DB::table('users')
            ->join('membros', 'users.id','=','membros.user_id')
            ->whereOrquestraId($data['orquestra'])
            ->whereRoleId(3)
            ->select('users.id', 'users.name', 'users.email')
            ->distinct()
            ->get();

        return $this->sendResponse($membros->toArray(), 'Retrieved successfully.');
    }

    public function perfis(Request $request)
    {
        $data = $request->all();
        $perfis = Membro::whereUserId($data['id'])->whereOrquestraId($data['orquestra'])->with(['role', 'instrumento'])->get();
        return $this->sendResponse($perfis, 'Retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     * Adiciona um novo membro para a orquestra, nele verifica se o usuario já existe,
     * caso sim cria r o membro com o perfil de visitante sem instrumento.
     * Caso não, criar usuario, cria o membro com o perfil de visitante e sem instrumento
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $instrumento = ($data['membro']['instrumento']) ? $data['membro']['instrumento'] : null;

        $membro = Membro::whereUserId($data['membro']['usuario'])
            ->whereOrquestraId($data['orquestra'])
            ->whereRoleId($data['membro']['role'])
            ->whereInstrumentoId($instrumento)
            ->get();

        if(count($membro) > 0) {
            return $membro;
        }

        $membro = Membro::create([
            'user_id' => $data['membro']['usuario'],
            'orquestra_id' => $data['orquestra'],
            'role_id' => $data['membro']['role'],
            'instrumento_id' => $instrumento,
        ]);

        $perfis = Membro::whereUserId($membro->user_id)->with(['orquestra', 'role', 'instrumento'])->orderBy('orquestra_id')->get();

        return $this->sendResponse(compact('membro', 'perfis'), 'Membro created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Membro $membro
     * @return \Illuminate\Http\Response
     */
    public function show(Membro $membro)
    {

        if (is_null($membro)) {
            return $this->sendError('Perfil not found. show');
        }

        return $this->sendResponse($membro->toArray(), 'Perfil retrieved successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Membro $membro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Membro $membro)
    {
        $usuario = User::find($membro->user_id);

        if (Membro::destroy($membro->id)) {

            $perfis = Membro::whereUserId($usuario->id)->with(['orquestra', 'role', 'instrumento'])->orderBy('orquestra_id')->get();

            if($usuario->photo)
                $usuario->photo  = asset('storage/' . $usuario->photo);

            return $this->sendResponse(compact('usuario', 'perfis'), Response::HTTP_OK);
        }
        return $this->sendError('Perfil not found.');
    }
}
