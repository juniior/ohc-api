<?php

namespace App\Http\Controllers;

use App\Musica;
use App\Partitura;
use App\PartituraInstrumento;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PartituraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $instrumentos = json_decode($request->instrumentos);
        $musica = json_decode($request->musica);
        $file = $request->file('file');

        $path = 'orquestra/' . $musica->orquestra_id . '/partituras/';
        $filename = $musica->id . '-' . time() . '.' . $file->getClientOriginalExtension();

        if (Storage::disk('public')->put($path . $filename, File::get($file))) {

            $partitura = Partitura::create([
                'musica_id' => $musica->id,
                'file' => $path . $filename,
                'mime' => $file->getClientMimeType(),
            ]);

            foreach ($instrumentos as $id) {
                PartituraInstrumento::create([
                    'instrumento_id' => $id,
                    'partitura_id' => $partitura->id
                ]);
            }
        }

        return $this->sendResponse(Musica::find($musica->id)->partituras, Response::HTTP_NO_CONTENT);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Partitura $partitura
     * @return \Illuminate\Http\Response
     */
    public function show(Partitura $partitura)
    {
        $conteudo = Storage::disk('public')->path($partitura->file);

        $pdf = file_get_contents($conteudo);
        $base64 = base64_encode($pdf);

        return $base64;


        return $this->sendResponse($base64, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Partitura $partitura
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partitura $partitura)
    {
        $partitura->delete();
        return $this->sendResponse(Musica::find($partitura->musica_id)->partituras, Response::HTTP_NO_CONTENT);
    }
}
