<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    protected $table = 'roles';

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name'
    ];

    public function membros() {
        return $this->hasMany(Membro::class);
    }
}
