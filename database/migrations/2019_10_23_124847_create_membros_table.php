<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('orquestra_id')->nullable()->index()->unsigned();
            $table->integer('role_id')->index()->unsigned();
            $table->integer('user_id')->index()->unsigned();
            $table->integer('instrumento_id')->nullable()->index()->unsigned();
            $table->boolean('is_default')->default(false)->comment('Flag para setar o perfil de entrada no sistema');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('orquestra_id')->references('id')->on('orquestras');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('instrumento_id')->references('id')->on('instrumentos');

//            $table->unique(['orquestra_id', 'role_id', 'user_id', 'instrumento_id']);

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membros');
    }
}
