<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitacoes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('orquestra_id')->nullable()->index()->unsigned();
            $table->integer('user_id')->index()->unsigned();
            $table->string('status', 30)->default('PENDENTE')->comment('Status da solicitação: ACEITO, CANCELADO, PENDENTE, RECUSADO');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('orquestra_id')->references('id')->on('orquestras');

            $table->unique(['orquestra_id', 'user_id']);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitacoes');
    }
}
