<?php

namespace App\Http\Controllers;

use App\Membro;
use App\Orquestra;
use App\Solicitacao;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SolicitacaoController extends Controller
{
    //ACEITO, CANCELADO, PENDENTE, RECUSADO
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'SolicitacaoController.index';
//        return $this->sendResponse(Solicitacao::orderBy('name')->paginate(25)->toArray(), 'Orquestras retrieved successfully.');
    }

    public function listaPorUsuario($user)
    {
        $solicitacoes = Solicitacao::with(['orquestra'])->where('user_id', $user)->paginate(25)->toArray();

        return $this->sendResponse(compact('solicitacoes'), 'Orquestras retrieved successfully.');
    }

    public function listaPorOrquestra($orquestra)
    {
        $solicitacoes = Solicitacao::with(['usuario'])
            ->where('orquestra_id', $orquestra)
            ->paginate(25)
            ->toArray();

        return $this->sendResponse(compact('solicitacoes'), 'Orquestras retrieved successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $orquestra = Orquestra::where('codigo', $data['codigo'])->first();
        $user = User::find($data['user']);

        $sol = Solicitacao::where('orquestra_id', $orquestra->id)
            ->where('user_id', $user->id)
            ->where('status', '!=', 'CANCELADO')
            ->first();

        if (isset($sol)) {
            return $this->sendResponse($sol->toArray(), 'Solicitacao ja existente');
        }

        $item = Solicitacao::create([
            'orquestra_id' => $orquestra->id,
            'user_id' => $user->id
        ]);

        return $this->sendResponse($item->toArray(), 'Solicitação retrieved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Solicitacao $solicitacao
     * @return \Illuminate\Http\Response
     */
    public function show(Solicitacao $solicitacao)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Solicitacao $solicitacao
     * @return \Illuminate\Http\Response
     */
    public function edit(Solicitacao $solicitacao)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Solicitacao $solicitacao
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Solicitacao $solicitacao)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Solicitacao $solicitacao
     * @return \Illuminate\Http\Response
     */
    public function destroy(Solicitacao $solicitacao)
    {
        $solicitacao->status = 'CANCELADO';
        $solicitacao->save();

        return $this->sendResponse($solicitacao->toArray(), Response::HTTP_OK);

    }

    public function aceitar(Request $request)
    {
        $data = $request->all();
        $solicitacao = Solicitacao::find($data['id']);
        $solicitacao->status = 'ACEITO';
        $solicitacao->save();

        Membro::create([
            'user_id' => $solicitacao->user_id,
            'orquestra_id' => $solicitacao->orquestra_id,
            'role_id' => 4,
            'instrumento_id' => null,
        ]);

        return $this->sendResponse($solicitacao->toArray(), 'Solicitação confirmada!');
    }

    public function recusar(Request $request)
    {
        $data = $request->all();
        $solicitacao = Solicitacao::find($data['id']);
        $solicitacao->status = 'RECUSADO';
        $solicitacao->save();

        return $this->sendResponse($solicitacao->toArray(), 'Solicitação recusada!');
    }
}
