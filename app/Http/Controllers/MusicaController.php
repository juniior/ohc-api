<?php

namespace App\Http\Controllers;

use App\Musica;
use App\Orquestra;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator, DB;

class MusicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $musicas = Musica::orderBy('name')->select('id', 'name', 'tonality', 'composer', 'orquestra_id')->get();
        return $this->sendResponse($musicas, 'Retrieved successfully.');
    }

    public function listaPorOrquestra(Orquestra $orquestra)
    {
        $musicas = Musica::whereOrquestraId($orquestra->id)->orderBy('name')->select('id', 'name', 'tonality', 'composer', 'orquestra_id')->get();
        return $this->sendResponse($musicas, 'Retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->musica, [
            'name' => 'required|string|max:200',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $musica = Musica::create([
            'name' => $request->musica['name'],
            'tonality' => $request->musica['tonality'],
            'composer' => $request->musica['composer'],
            'letter' => $request->musica['letter'],
            'orquestra_id' => $request->orquestra,
        ]);

        $musicas = Musica::whereOrquestraId($musica->orquestra_id)->orderBy('name')->select('id', 'name', 'tonality', 'composer', 'orquestra_id')->get();

        return $this->sendResponse(compact('musica', 'musicas'), Response::HTTP_NO_CONTENT);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Musica $musica
     * @return \Illuminate\Http\Response
     */
    public function show(Musica $musica)
    {
        $partituras = $musica->partituras;
        return $this->sendResponse(compact('musica', 'partituras'), Response::HTTP_NO_CONTENT);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Musica $musica
     * @return \Illuminate\Http\Response
     */
    public function edit(Musica $musica)
    {
        return $this->sendResponse($musica->toArray(), Response::HTTP_NO_CONTENT);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Musica $musica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Musica $musica)
    {
        $musica->name = $request->name;
        $musica->composer = $request->composer;
        $musica->tonality = $request->tonality;
        $musica->letter = $request->letter;

        $musica->save();

        $musicas = Musica::whereOrquestraId($musica->orquestra_id)->orderBy('name')->select('id', 'name', 'tonality', 'composer', 'orquestra_id')->get();
        return $this->sendResponse(compact('musica', 'musicas'), Response::HTTP_NO_CONTENT);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Musica $musica
     * @return \Illuminate\Http\Response
     */
    public function destroy(Musica $musica)
    {
        $musica->delete();
        $musicas = Musica::whereOrquestraId($musica->orquestra_id)->orderBy('name')->select('id', 'name', 'tonality', 'composer', 'orquestra_id')->get();
        return $this->sendResponse(compact('musicas'), Response::HTTP_NO_CONTENT);
    }

    public function allPartituras(Musica $musica)
    {
        return $this->sendResponse($musica->partituras, Response::HTTP_NO_CONTENT);
    }
}
